# 8 week 2019-05-26

## Algorithm 算法
字符串低位，高位优先排序


## Review 英文技术文章
node更好的log
[原文地址][1]


## Tip 技术点
阅读《重构》第八九十章


## Share 分享文章
前端自检清单，还是有点参考价值的[原文地址][6]

## Hot 热点
- [flutter-web][2] 有可能是更好的全端方式
- [terminal][3] window系统的terminal
- [vscode remote][4] 远程编辑器
- [react-native-windows][5] RN重写window应用


[1]: https://www.twilio.com/blog/guide-node-js-logging
[2]: https://github.com/flutter/flutter_web#at?hmsr=toutiao.io&utm_medium=toutiao.io&utm_source=toutiao.io
[3]: https://github.com/microsoft/Terminal#at?hmsr=toutiao.io&utm_medium=toutiao.io&utm_source=toutiao.io
[4]: https://github.com/microsoft/vscode-remote-release
[5]: https://github.com/microsoft/react-native-windows
[6]: https://mp.weixin.qq.com/s/SJVKl-cTpqIz1vcUgNy_Cw